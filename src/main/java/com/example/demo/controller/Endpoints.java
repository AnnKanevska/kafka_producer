package com.example.demo.controller;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.Set;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Endpoints {

    public static final String PURCHASE_ORDERS = "purchaseOrder";

    public static final Set<String> values = Collections.singleton(PURCHASE_ORDERS);
}
