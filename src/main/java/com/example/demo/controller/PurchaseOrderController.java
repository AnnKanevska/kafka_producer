package com.example.demo.controller;

import com.example.demo.domain.dto.PoServiceExternalEventResponseDto;
import com.example.demo.service.KafkaService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping(Endpoints.PURCHASE_ORDERS)
@Slf4j
public class PurchaseOrderController {
     private final KafkaService kafkaService;
    @PostMapping
    public void getPurchaseOrders(@RequestBody PoServiceExternalEventResponseDto purchaseOrderResponseDto) {
       kafkaService.sendMessageToPrimaryTopic(purchaseOrderResponseDto);
    }
}
