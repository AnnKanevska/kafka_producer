package com.example.demo.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DistributeLocationDto {
    private Long id;
    private Boolean nonScalingIndicator;
    private String externalDistributionId;
    private Long quantityAllocated;
    private Long quantityPrescaled;
    private Long quantityTransferred;
    private Long quantityReceived;
    private Long quantityCanceled;
    private Long shipLocationId;
    private String distributionId;
}
