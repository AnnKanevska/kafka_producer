package com.example.demo.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExternalEventResponseMetadataDto {
    private Long revisionId;
    private String action;
    private String consumerName;
    private String processingStatus;
    private ZonedDateTime commitTimestamp;
    private ZonedDateTime targetSystemCommitTimestamp;
    private ZonedDateTime eventReceivedTimestamp;
    private Long eventId;
    private String correlationId;
    private String externalTarget;
}
