package com.example.demo.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ItemDto {
    private Boolean casePackIndicator;
    private String costSource;
    private LocalDate earliestShipDate;
    private String externalItemId;
    private Double initialUnitCost;
    private LocalDate latestShipDate;
    private Boolean nonScalingIndicator;
    private String originCountry;
    private String referenceItem;
    private Long supplierPackSize;
    private Double unitCost;
    private String universalProductCode;
    private String universalProductCodeSupplement;
    private List<DistributeLocationDto> distributeLocations;
    private List<ShipLocationDto> shipLocations;
    private String itemId;


}
