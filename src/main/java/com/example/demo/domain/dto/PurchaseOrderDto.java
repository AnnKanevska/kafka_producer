package com.example.demo.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseOrderDto {

    private Long id;
    private Boolean agentEdiIndicator;
    private Boolean consolidatorEdiIndicator;
    private String currency;
    private Boolean dropShipIndicator;
    private LocalDate earliestShipDate;
    private Boolean ediIndicator;
    private Boolean ediSentIndicator;
    private LocalDate endShipDate;
    private Boolean excludeFromAvailabilityIndicator;
    private String externalId;
    private Boolean importFullEdiManufacturerSentIndicator;
    private Boolean importOrderIndicator;
    private Boolean includeOnOrderIndicator;
    private LocalDate latestShipDate;
    private Boolean manufacturerEdiIndicator;
    private Boolean nordstromComIndicator;
    private Boolean nordstromProductGroupIndicator;
    private LocalDate openToBuyEndOfWeekDate;
    private String orderFromVendorId;
    private String orderType;
    private Long originIndicator;
    private Boolean outOfCountryIndicator;
    private Boolean preMarkIndicator;
    private Boolean qualityCheckIndicator;
    private LocalDate startShipDate;
    private String terms;
    private Boolean ticketEdiIndicator;
    private Integer departmentId;
    private String dischargeEntryPort;
    private String purchaseOrderType;
    private String paymentMethod;
    private String accountsPayableComments;
    private String buyerId;
    private String comments;
    private String crossReferenceExternalId;
    private Double exchangeRate;
    private String importCountry;
    private LocalDate importFullEdiManufacturerDate;
    private String ladingPort;
    private String manufacturerComments;
    private LocalDate manufacturerEarliestShipDate;
    private LocalDate manufacturerLatestShipDate;
    private String nordstromProductGroupCostCenter;
    private String plannerId;
    private String promotionId;
    private String purchaseType;
    private LocalDate ticketEdiDate;
    private LocalDate ticketRetailDate;
    private String titlePassResponsibility;
    private String brokerId;
    private String buyingAgentId;
    private String consolidatorId;
    private String countryOfManufacturer;
    private String factoryId;
    private String freightPaymentMethod;
    private String freightTerms;
    private String manufacturerId;
    private String manufacturerPaymentMethod;
    private String manufacturerPaymentTerms;
    private String secondaryFactoryId;
    private String shippingMethod;
    private String ticketFormat;
    private String transportationResponsibility;
    private LocalDate inStoreScheduledDate;
    private String packingMethod;
    private Long planSeasonId;
    private Long ticketPartnerId;
    private String vendorPurchaseOrderId;
    private LocalDate originalApprovalDate;
    private String originalApprovalId;
    private Boolean acknowledgementReceivedIndicator;
    private Long orderFromVendorAddressSequenceNumber;
    private String openToBuyEndOfWeekDateChangedBy;
    private LocalDate openToBuyEndOfWeekChangedDate;
    private LocalDate writtenDate;
    private LocalDate closeDate;
    private LocalDate pickupDate;
    private String status;
    private String rackCompareAtSeasonCode;
    private List<ItemDto> items;
}
