package com.example.demo.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ShipLocationDto {
    private Long id;
    private String cancelCode;
    private LocalDate cancelDate;
    private String canceledById;
    private LocalDate estimatedInStockDate;
    private Long lastReceived;
    private Boolean nonScalingIndicator;
    private Long originalReplenishQuantity;
    private Long quantityCanceled;
    private Long quantityOrdered;
    private Long quantityPrescaled;
    private Long quantityReceived;
    private Long quantityReceivedAllocatedNotReleased;
    private Double unitRetail;
    private String distributionDescription;
    private String distributionMethod;
    private String distributionId;
    private String distributionStatus;
    private String externalDistributionId;
    private LocalDate releaseDate;

}
