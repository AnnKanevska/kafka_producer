package com.example.demo.service;

import com.example.demo.domain.dto.PoServiceExternalEventResponseDto;

public interface KafkaService {
    void sendMessageToPrimaryTopic(PoServiceExternalEventResponseDto purchaseOrderResponseDto);
}
