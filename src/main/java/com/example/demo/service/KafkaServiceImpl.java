package com.example.demo.service;

import com.example.demo.domain.dto.PoServiceExternalEventResponseDto;
import com.example.demo.service.stream.EventsBinding;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
@Slf4j
public class KafkaServiceImpl implements KafkaService {

    private final EventsBinding eventsBinding;


    public void sendMessageToPrimaryTopic(PoServiceExternalEventResponseDto purchaseOrderResponseDto) {
        Message<PoServiceExternalEventResponseDto> message = MessageBuilder.withPayload(purchaseOrderResponseDto).build();
        try {
            boolean sent = eventsBinding.output().send(message);
            if (sent) {
                log.info("Message was successfully sent to Kafka topic with headers with PO id: {}",
                        purchaseOrderResponseDto.getPurchaseOrder().getId());
            } else {
                log.info("Message with PO id: {}.", purchaseOrderResponseDto.getPurchaseOrder().getId());
            }
        } catch (Exception ex) {
            log.warn("Failed to send a message to Kafka topic with headers with PO id: {}", purchaseOrderResponseDto.getPurchaseOrder().getId());

        }
    }
}
