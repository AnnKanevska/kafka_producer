package com.example.demo.service.stream;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface EventsBinding {
    String EVENTS = "events";

    @Output(EVENTS)
    MessageChannel output();
}

